/**
 * Grid Life Helpers
 */

 function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function getSeedGrid(rows, cols, radius) {
  var seedGrid = [];
  for (var r = 0; r < rows; ++r) {
    seedGrid.push([]);
    for (var c = 0; c < cols; ++c) {
      var life = getRandomInt(2);
      seedGrid[r].push({
        life,
        radiusCurrent: life === 1.0 ? radius : 0.0
      });
    }
  }
  return seedGrid;
}

function makeGetNextLifeCell(getNextLife, rows, cols) {
  function getNextLifeCell(previousGrid, r, c) {
    var count = 0;
    for (var ri = r - 1; ri <= r + 1; ++ri) {
      for (var ci = c - 1; ci <= c + 1; ++ci) {
        if (ri === r && ci === c)
          continue;

        var rt = ri;
        if (rt < 0)
          rt += rows;
        else if (rt >= rows)
          rt -= rows;

        var ct = ci;
        if (ct < 0)
          ct += cols;
        else if (ct >= cols)
          ct -= cols;

        count += previousGrid[rt][ct].life;
      }
    }
    
    return {
      life: getNextLife(previousGrid[r][c].life, count),
      radiusCurrent: previousGrid[r][c].radiusCurrent
    };
  }

  return getNextLifeCell;
}

function makeGetNextRadiusCell(radius, radiusIncrement) {
  function getNextRadiusCell(previousGrid, r, c) {
    var life = previousGrid[r][c].life;
    var radiusCurrent = previousGrid[r][c].radiusCurrent;

    if (life === 1 && radiusCurrent < radius)
      radiusCurrent += radiusIncrement;
    else if (life === 0 && radiusCurrent > 0.0)
      radiusCurrent -= radiusIncrement;

    return {
      life,
      radiusCurrent
    };
  }

  return getNextRadiusCell;
}

function getNextGrid(previousGrid, rows, cols, getNextCell) {
  var nextGrid = [];
  for (var r = 0; r < rows; ++r) {
    nextGrid.push([]);
    for (var c = 0; c < cols; ++c) {
      nextGrid[r].push(getNextCell(previousGrid, r, c));
    }
  }

  return nextGrid;
}

function makeDrawRectangle(context, rows, cols, width, height, radius, radiusIncrement) {
  function drawRectangle(grid, r, c, color) {
    var radiusCurrent = grid[r][c].radiusCurrent;
    if (radiusCurrent < radiusIncrement)
      return;

    var rad = (radius / 2) - (radiusCurrent / 2);
    var y = (r / rows) * height + rad;
    var x = (c / cols) * width + rad;

    context.beginPath();
    context.rect(x, y, radiusCurrent, radiusCurrent);
    context.fillStyle = color;
    context.fill();
  }

  return drawRectangle;
}

function drawForeground(grid, rows, cols, color, drawShape) {
  for (var r = 0; r < rows; ++r) {
    for (var c = 0; c < cols; ++c) {
      drawShape(grid, r, c, color);
    }
  }
}

function drawBackground(context, width, height, color) {
  context.clearRect(0, 0, width, height);
  context.beginPath();
  context.rect(0, 0, width, height);
  context.fillStyle = color;
  context.fill();
}


/**
 * Grid Life Rule Interpreter
 * 
 * format example for Conway's game: S23/B3
 * where 'S' is for survival and 'B' is for birth
 */

function getSingleDigitNumsOfRuleString(ruleString) {
  try {
    var ruleSplit = ruleString.split('');
    var ruleInts = ruleSplit.map(s => parseInt(s));
    var ruleFiltered = ruleInts.filter(i => Number.isInteger(i) && i >= 0 && i <= 8);
    return ruleFiltered;
  } catch (err) {
    console.error(err);
    return [];
  }
}

function makeGetNextLife(rule) {
  var messageInvalid = 'invalid rule applied, using rule S23/B3 to proceed';
  var survival = [2, 3];
  var birth = [3];

  try {
    var ruleTrimmed = rule.replace('S', '').replace('B', '').replace(' ', '');
    var ruleSplit = ruleTrimmed.split('/');
    
    if (ruleSplit.length < 2 || ruleSplit[0].length < 1 || ruleSplit[1].length < 1) {
      console.log(messageInvalid);
    } else {
      var sur = getSingleDigitNumsOfRuleString(ruleSplit[0]);
      var bir = getSingleDigitNumsOfRuleString(ruleSplit[1]);

      if (sur.length < 1 || bir.length < 1) {
        console.log(messageInvalid);
      } else {
        survival = sur;
        birth = bir;
      }
    }
  } catch (err) {
    console.log(messageInvalid);
    console.error(err);
  }

  function getNextLife(life, count) {
    var nextLife = 0;
    if (life === 1 && survival.indexOf(count) !== -1)
      nextLife = 1;
    else if (life === 0 && birth.indexOf(count) !== -1)
      nextLife = 1;

    return nextLife;
  }

  return getNextLife;
}


/**
 * Grid Life Animation
 */

function makeAnimate(canvasId, getNextLife, backgroundColor, foregroundColor, rows, steps) {
  var canvas = document.getElementById(canvasId);
  var context = canvas.getContext("2d");

  var width = canvas.width;
  var height = canvas.height;

  var cols = Math.floor(rows * width / height);

  var radius = width > height ? (0.5 / rows) * height : (0.5 / cols) * width;
  var radiusIncrement = radius / steps;
  var stepsIncrement = 1;

  var grid = getSeedGrid(rows, cols, radius);

  var drawRectangle = makeDrawRectangle(context, rows, cols, width, height, radius, radiusIncrement);
  var getNextRadiusCell = makeGetNextRadiusCell(radius, radiusIncrement);
  var getNextLifeCell = makeGetNextLifeCell(getNextLife, rows, cols);

  function animate() {
    drawBackground(context, width, height, backgroundColor);
    drawForeground(grid, rows, cols, foregroundColor, drawRectangle);

    grid = stepsIncrement < 2 * steps
      ? getNextGrid(grid, rows, cols, getNextRadiusCell)
      : getNextGrid(grid, rows, cols, getNextLifeCell);

    stepsIncrement = stepsIncrement >= 2 * steps
      ? 1
      : stepsIncrement + 1;
  }

  return animate;
}
